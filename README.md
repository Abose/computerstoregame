
# Computerstore - minigame

this is a project to show what we have learned in html, css but mostly in javascript 

Made responsive for phones, tablets and desktop 

Start with timer for work is 30 second everytime you press Training it goes down 1 sec and Studying make you get more money its not a bug but a feature..

Work button, Studying button can only be pressed 10 times everyday before you have to sleep 

You cannot study more then 5 times everyday

You will have to eat 3 times a day before you can sleep



## Installation

[Live preview running](https://computerstore.abose.synology.me/) -- nb had to change folder structure to make it work on my server 

Install minigame with gitclone

```bash
  git clone git@gitlab.com:Abose/computerstoregame.git
  cd computerstoregame
  and run index.html or use live Service
  
  or

  download zip 
  extract 
  and run index.html 
```


    
## Screenshots

![App Screenshot](https://i.ibb.co/BfnvNLw/127-0-0-1-5500-computerstoregame-index-html-full-HD.png)
![App Screenshot](https://i.ibb.co/Nx9SccS/127-0-0-1-5500-computerstoregame-index-html.png)



## 🚀 About Me
i am going on a noroff course to become full stack developer...


## 🛠 Skills
Javascript, HTML, CSS...


## Features

- Live previews
- Fullscreen mode
- Cross platform


## Lessons Learned

What did you learn while building this project? What challenges did you face and how did you overcome them?

First challenge where how to get value back from selector 

second challenge how to show the right json content to the html dom 

third how to fix a broken link from the json

make everything looks pretty depend on which device you use



## 🔗 Links
[![portfolio](https://img.shields.io/badge/my_portfolio-000?style=for-the-badge&logo=ko-fi&logoColor=white)](https://portfolio.abose.synology.me/)
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](www.linkedin.com/in/abbas-mehdi-shihab)



## License

[MIT](https://choosealicense.com/licenses/mit/)

