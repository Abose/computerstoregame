// default values
let balance = 200;
let work = 100;
let workCounter = 0;
let pay = 0;
let loan = 0;
let loanPayBackPay = 0;
const rate = 0.1;
let moneyTake = 0;
let studying = 1;
let studyingCounter = 1;
let training = 1;
let trainingCounter = 1;
let trainingCounterTotal = 1;
let amount = 0;
let possibleLoan = balance * 2;
let loanAble = true;
let loanCounter = 0;
let value = 0;
let activeLaptop = 1; // kan kanskje brukes til å store stock etter forandring
let defaultTraining = 30000;
let inStock = 0;
let eat = 100;
let eatLast = 1;
let sleep = 0;
// default values

// default name
const loanText = document.getElementById("loanid");
const balanceLive = document.getElementById("balancecurrently");
const loanLive = document.getElementById("loanamount");
const loanButton = document.getElementById("btnpayloan");
const loanButtonPay = document.getElementById("loanButtonPay");
const btnBank = document.getElementById("btnbank");
const btnLoan = document.getElementById("btnloan");
const payBalance = document.getElementById("payBalance");
const btnWork = document.getElementById("btnwork");
const buyStatus = document.getElementById("buystatus");
const trainingLvl = document.getElementById("trainingLvl");
const stydingLvl = document.getElementById("StydingLvl");
const btnStudying = document.getElementById("btnStudying");
const btnTraining = document.getElementById("btnTraining");
const btnSleep = document.getElementById("btnSleep");
const btnEat = document.getElementById("btnEat");
const eatCounter = document.getElementById("eatCounter");
const daysSleep = document.getElementById("daysSleep");
const statusText = document.getElementById("statusText");

// default name

// laptop selector
const id = document.getElementById("id");
const title = document.getElementById("title");
const description = document.getElementById("description");
const specs = document.getElementById("specs");
let price = document.getElementById("price");
const stock = document.getElementById("stock");
const active = document.getElementById("active");
const image = document.getElementById("image");
const selectortitle = document.getElementById("selectortitle");
const apiurl = "https://noroff-komputer-store-api.herokuapp.com/";
// laptop selector

// loan getbutton //
stydingLvl.innerHTML = "lvl " + studying;
trainingLvl.innerHTML = "lvl " + training;
function takeLoan() {
  let amount = prompt("Enter the amount of loan");
  amount = Number(amount);
  possibleLoan = balance * 2;
  if (amount == null || amount == "" || amount < 0 || amount < 100) {
    text = "User cancelled the prompt.";
    loanText.innerHTML = text;
    return;
  }
  if (amount <= possibleLoan && loanAble) {
    balance = amount + balance;
    loan += amount;
    amount = 0;
    loanAble = false;
    balanceLive.innerHTML = balance + "kr";
  }
  if (loanAble != true) {
    if (loanCounter > 0) {
      text = "Cant have multiple loans!";
      loanText.innerHTML = text;
      return;
    }
    text = "Enjoy the loan";
    loanText.innerHTML = text;
    text = "Loan remains &nbsp; &nbsp;	&nbsp;" + loan;
    loanLive.innerHTML = text;
    loanAble = false;
    loanCounter += 1;
    return;
  }
  if (amount > possibleLoan) {
    text = "Dont be Greedy!";
    loanText.innerHTML = text;
    return;
  } else {
    amount = 0;
    balanceLive.innerHTML = balance + "kr";
    return;
  }
}
// loan getbutton //

// loan paybutton //
function payLoan() {
  if (loan > balance) {
    text = "Not enough money to payback";
    loanText.innerHTML = text;
  }
  if (loan <= balance) {
    paybackloanfull = balance - loan;
    balance += paybackloanfull - balance;
    loanButton.className = "invisible";
    loanButtonPay.className = "invisible";
    balanceLive.innerHTML = balance + "kr";
    text = "paid in full nice!!";
    loanText.innerHTML = text;
    btnLoan.className = "btn-loan";
    btnBank.className = "btn-bank";
    loanCounter = 0;
    loanAble = true;
    loan = 0;
  }
}
// loan paybutton //

// check and makes changes to layout depends on have loan or not
let loanchecker = window.setInterval(function () {
  if (loan == 0) {
    loanAble = true;
    loanCounter = 0; // lagt til denne
    loanButton.className = "invisible";
    loanButtonPay.className = "invisible";
    text = "";
    loanLive.innerHTML = text;
    return;
  }
  if (loanAble != true) {
    if (loan > 0) {
      text = "Loan remains &nbsp; &nbsp;	&nbsp;" + loan;
      loanLive.innerHTML = text;
    }
    loanButton.className = "btn-loanpay";
    loanButtonPay.className = "btn-loanpaypay";
    btnLoan.className = "btn-loanactive";
    btnBank.className = "btn-bankactive";

    return;
  }
}, 200);
let loanmessage = window.setInterval(function () {
  if (loan == 0) {
    text = "Possible to take loan!";
    loanText.innerHTML = text;
    text = "";
    buyStatus.innerHTML = text;
    return;
  }
}, 8000);

text = "Possible to take loan!";
loanText.innerHTML = text;
// check and makes changes to layout depends on have loan or not

// give balance, eatcounter and statustext headstart
eatCounter.innerText = eatLast;
balanceLive.innerHTML = balance + "kr";
statusText.innerText = "Time to wake up and work";
// give balance headstart with 200kr

// Work button // adds the amount of payment everytime when pressed (pay += work;)
function makeMoney() {
  if (workCounter > 10) {
    statusText.innerText = "You should sleep.. cant work anymore";
    return;
  }
  workCounter += 1;
  pay += work;
  payBalance.innerHTML = pay + "kr";
  statusText.innerText = "Working hard arent we..";
}
// Work button // adds the amount of payment everytime when pressed (pay += work;)

// Bank Button // send your money to balance everytime its pressed (balance += pay;)
function getMoney() {
  if (loan > 0) {
    moneyTake = pay * rate;
    loanPayBackPay = moneyTake + loanPayBackPay;
    loan = loan - loanPayBackPay;
    loanPayBackPay = loanPayBackPay - loanPayBackPay;
    pay = pay - moneyTake;
    text = "Loan remains &nbsp; &nbsp;	&nbsp;" + loan;
    loanLive.innerHTML = text;
  }
  if (loan < 0) {
    if (loan < 0) {
      balance += pay;
      loan -= loan + loan;
      balance += loan;
      loanAble = true;
      text = "Possible to take loan!";
      loanText.innerHTML = text;

      console.log("our Bank made a mistake money sent back");
    }
    loan = 0;
  } else {
    balance += pay;
    if (pay > 10) {
      statusText.innerText = "Sweet!";
    }
  }
  balanceLive.innerHTML = balance + "kr";
  pay = 0;
  payBalance.innerHTML = pay;
  if (loan == 0) {
    btnLoan.className = "btn-loan";
    btnBank.className = "btn-bank";
    loanButton.className = "invisible";
    loanButtonPay.className = "invisible";
  }
}
// Bank Button // send your money to balance everytime its pressed (balance += pay;)

// working and training button
function trainingButton() {
  if (trainingCounterTotal > 30) {
    statusText.innerText = "Max training good job!";
    return;
  } else if (trainingCounter > 9) {
    statusText.innerText = "You should sleep.. cant workout anymore";
    return;
  }
  defaultTraining -= 1000;
  training += 1;
  trainingCounterTotal += 1;
  trainingCounter += 1;
  trainingLvl.innerHTML = "lvl " + training;
  statusText.innerText = "Dem Gains!";
}
function studyingButton() {
  if (studyingCounter > 4) {
    statusText.innerText = "You should sleep.. cant Study anymore";
    return;
  }
  work += 100;
  studyingCounter += 1;
  studying += 1;
  stydingLvl.innerHTML = "lvl " + studying;
  statusText.innerText = "Dem Brains!";
}
// working and training button

// added Timer on Work Button
function workButton(id) {
  btnWork.disabled = true;
  setTimeout(function () {
    btnWork.className = "btn-work";
    btnWork.disabled = false;
  }, defaultTraining);
  btnWork.className = "disabled";
}
btnWork.addEventListener("click", workButton);
// added Timer on Work Button

// give the value of which of the selector is selected and pass it on unto fetchlaptops function
const selectpc = document.querySelector("#choice");
const changeHandler = (ev) => {
  value = ev.target.value;
  value = Number(value);
  text = "";
  buyStatus.innerHTML = text;
  fetchLaptops();
};
selectpc.addEventListener("change", changeHandler);
// give the value of which of the selector is selected and pass it on unto fetchlaptops function

// json extract data and add it to the spesifik place
async function fetchLaptops() {
  try {
    const response = await fetch(
      "https://noroff-komputer-store-api.herokuapp.com/computers"
    );
    // waits until the request completes...
    let data = await response.json();

    inStock = data[value]["stock"];
    title.innerHTML = data[value]["title"];
    description.innerHTML = data[value]["description"];
    specs.innerHTML = data[value]["specs"];
    stock.innerHTML = "stock : " + inStock;
    image.src = apiurl + data[value]["image"];
    price.innerHTML = data[value]["price"];

    if (value == 4) {
      // json file give the wrong url fixed it only for value 4
      image.src = apiurl + "/assets/images/5.png";
    }
  } catch (exp) {
    console.log("error");
  }
}
fetchLaptops();
// json extract data and add it to the spesific place

// buy button
function Buylaptop() {
  priceProduct = price.innerText;
  priceProduct = Number(priceProduct);
  moneyAfterBuy = balance - priceProduct;
  if (priceProduct > balance) {
    text = "You dont have enough missing ";
    buyStatus.innerHTML = text + moneyAfterBuy;
  }
  if (priceProduct <= balance) {
    if (inStock <= 0) {
      text = "Not in stock!";
      buyStatus.innerHTML = text;
      stock.innerHTML = "stock : " + inStock;
      return;
    }

    balance += moneyAfterBuy - balance;
    text = "Enjoy item!";
    buyStatus.innerHTML = text;
    inStock -= 1;
    stock.innerHTML = "stock : " + inStock;
  }
  moneyAfterBuy = 0;
  balanceLive.innerHTML = balance + "kr";
}
// buy button

// eat Button and Sleep Button
function eatButton() {
  if (balance > 0) {
    if (eatLast == 3) {
      statusText.innerText = "Too full i guess i will Eat tomorrow!";
      return;
    }
    balance -= eat;
    balanceLive.innerHTML = balance + "kr";
    eatLast += 1;
    eatCounter.innerText = eatLast;
    statusText.innerText = "That was Delicious well Deserved!";
    return;
  }
  statusText.innerText = "Dont have Enough money to eat.. F";
}
function sleepButton() {
  if (eatLast < 3) {
    statusText.innerText = "Too hungry to sleep!";
    return;
  }
  sleep += 1;
  eatLast = 0;
  studyingCounter = 0;
  trainingCounter = 0;
  workCounter = 0;
  eatCounter.innerText = eatLast;
  daysSleep.innerText = sleep;
  statusText.innerText = "New day new possibilities!";
}
// eat Button and Sleep Button
// pay loan button on work section
function payLoanfromwork() {
  if (loan > pay) {
    text = "Not enough money to payback";
    statusText.innerText = text;
  }
  if (loan <= pay) {
    paybackloanfull = pay - loan;
    balance += paybackloanfull;
    loanButton.className = "invisible";
    loanButtonPay.className = "invisible";
    balanceLive.innerHTML = balance + "kr";
    loanText.innerHTML = "paid in full nice!!";
    btnLoan.className = "btn-loan";
    btnBank.className = "btn-bank";
    statusText.innerText = "Nice paid in full from Working money";
    loanCounter = 0;
    loanAble = true;
    loan = 0;
    pay = 0;
    payBalance.innerHTML = pay + "kr";
  }
}
// pay loan button on work section
